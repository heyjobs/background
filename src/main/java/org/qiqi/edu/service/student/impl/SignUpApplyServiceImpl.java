package org.qiqi.edu.service.student.impl;

import org.qiqi.edu.common.QiQiException;
import org.qiqi.edu.entity.student.SignUpApplyEntity;
import org.qiqi.edu.jpa.student.ISignUpApplyRepository;
import org.qiqi.edu.service.student.ISignUpApplyService;
import org.qiqi.edu.utils.IdUtils;
import org.qiqi.edu.utils.easyquery.QueryUtils;
import org.qiqi.edu.utils.verify.VerifyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@Service
public class SignUpApplyServiceImpl implements ISignUpApplyService {

    private final ISignUpApplyRepository signUpApplyRepository;

    @Autowired
    public SignUpApplyServiceImpl(ISignUpApplyRepository signUpApplyRepository) {
        this.signUpApplyRepository = signUpApplyRepository;
    }

    @Override
    public List<SignUpApplyEntity> match(SignUpApplyEntity condition) {
        return QueryUtils.findMatch(condition, signUpApplyRepository);
    }

    @Override
    public void save(SignUpApplyEntity apply) throws QiQiException {
        apply.setApplyId(IdUtils.uuid());
        VerifyUtils.isBlack(apply.getAddress());
        VerifyUtils.isBlack(apply.getOpenId());
        VerifyUtils.isBlack(apply.getName());
        VerifyUtils.isBlack(apply.getPhone());
        signUpApplyRepository.save(apply);
    }
}
