package org.qiqi.edu.service.student;

import org.qiqi.edu.common.QiQiException;
import org.qiqi.edu.entity.student.SignUpApplyEntity;
import org.qiqi.edu.service.IBaseService;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public interface ISignUpApplyService extends IBaseService<SignUpApplyEntity> {
    void save(SignUpApplyEntity apply) throws QiQiException;
}
