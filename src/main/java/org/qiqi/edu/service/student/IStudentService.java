package org.qiqi.edu.service.student;

import org.qiqi.edu.entity.student.StudentEntity;
import org.qiqi.edu.service.IBaseService;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IStudentService extends IBaseService<StudentEntity> {
    List<StudentEntity> findByClassId(String classId);
    StudentEntity findById(String id);
}
