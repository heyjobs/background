package org.qiqi.edu.service.student.impl;

import org.qiqi.edu.entity.student.StudentEntity;
import org.qiqi.edu.jpa.student.IStudentRepository;
import org.qiqi.edu.service.IBaseService;
import org.qiqi.edu.service.student.IStudentService;
import org.qiqi.edu.utils.easyquery.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Service
public class StudentServiceImpl implements IStudentService {
    private final IStudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<StudentEntity> findByClassId(String classId) {
        return studentRepository.findByClassId(classId);
    }

    @Override
    public StudentEntity findById(String id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    public List<StudentEntity> match(StudentEntity condition) {
        return QueryUtils.findMatch(condition, studentRepository);
    }
}
