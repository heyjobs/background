package org.qiqi.edu.service.basedata.impl;

import org.qiqi.edu.entity.basedata.MenusEntity;
import org.qiqi.edu.jpa.basedata.IMenuRepository;
import org.qiqi.edu.service.basedata.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Service
public class MenuServiceImpl implements IMenuService {
    private final IMenuRepository menuRepository;

    @Autowired
    public MenuServiceImpl(IMenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public List<MenusEntity> findByActive(Boolean active) {
        return menuRepository.findByActive(true);
    }
}
