package org.qiqi.edu.service.basedata;

import org.qiqi.edu.entity.basedata.ClassEntity;
import org.qiqi.edu.service.IBaseService;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IClassService extends IBaseService<ClassEntity> {
    List<ClassEntity> findAll();
}
