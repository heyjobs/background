package org.qiqi.edu.service.basedata;

import org.qiqi.edu.entity.basedata.MenusEntity;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IMenuService {

    List<MenusEntity> findByActive(Boolean active);
}
