package org.qiqi.edu.service.basedata.impl;

import org.qiqi.edu.entity.basedata.ClassEntity;
import org.qiqi.edu.jpa.basedata.IClassRepository;
import org.qiqi.edu.service.basedata.IClassService;
import org.qiqi.edu.utils.easyquery.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Service
public class ClassServiceImpl implements IClassService {

    private final IClassRepository classRepository;

    @Autowired
    public ClassServiceImpl(IClassRepository classRepository) {
        this.classRepository = classRepository;
    }

    @Override
    public List<ClassEntity> findAll() {
        return classRepository.findAll();
    }

    @Override
    public List<ClassEntity> match(ClassEntity condition) {
        return QueryUtils.findMatch(condition, classRepository);
    }
}
