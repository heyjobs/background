package org.qiqi.edu.service.basedata.impl;

import org.qiqi.edu.entity.basedata.UserEntity;
import org.qiqi.edu.jpa.basedata.IUserRepository;
import org.qiqi.edu.service.basedata.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Service
public class UserServiceImpl implements IUserService {
    private final IUserRepository userRepository;

    @Autowired
    public UserServiceImpl(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void save(UserEntity user) {
        userRepository.save(user);
    }
}
