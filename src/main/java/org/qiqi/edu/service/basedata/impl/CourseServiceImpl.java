package org.qiqi.edu.service.basedata.impl;

import org.qiqi.edu.entity.basedata.CourseEntity;
import org.qiqi.edu.jpa.basedata.ICourseRepository;
import org.qiqi.edu.service.basedata.ICourseService;
import org.qiqi.edu.utils.easyquery.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@Service
public class CourseServiceImpl implements ICourseService {

    private final ICourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(ICourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<CourseEntity> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public List<CourseEntity> findByGrade(String grade) {
        return courseRepository.findByGrade(grade);
    }

    @Override
    public List<CourseEntity> match(CourseEntity condition) {
        return QueryUtils.findMatch(condition, courseRepository);
    }
}
