package org.qiqi.edu.service.basedata;

import org.qiqi.edu.entity.basedata.CourseEntity;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public interface ICourseService {
    List<CourseEntity> findAll();

    List<CourseEntity> findByGrade(String grade);
    List<CourseEntity> match(CourseEntity condition);
}
