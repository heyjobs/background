package org.qiqi.edu.service.basedata;

import org.qiqi.edu.entity.basedata.UserEntity;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IUserService {
    void save(UserEntity user);
}
