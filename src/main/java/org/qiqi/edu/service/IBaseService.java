package org.qiqi.edu.service;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public interface IBaseService<T> {
     List<T> match(T condition);
}
