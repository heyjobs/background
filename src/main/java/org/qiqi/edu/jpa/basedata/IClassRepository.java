package org.qiqi.edu.jpa.basedata;

import org.qiqi.edu.entity.basedata.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IClassRepository extends JpaRepository<ClassEntity, String>, JpaSpecificationExecutor<ClassEntity> {
}
