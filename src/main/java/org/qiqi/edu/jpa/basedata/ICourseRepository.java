package org.qiqi.edu.jpa.basedata;

import org.qiqi.edu.entity.basedata.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public interface ICourseRepository extends JpaRepository<CourseEntity,String>, JpaSpecificationExecutor<CourseEntity> {
    List<CourseEntity> findByGrade(String grade);
}
