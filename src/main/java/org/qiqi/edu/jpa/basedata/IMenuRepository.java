package org.qiqi.edu.jpa.basedata;

import org.qiqi.edu.entity.basedata.MenusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IMenuRepository extends JpaRepository<MenusEntity,String> {
    List<MenusEntity> findByActive(Boolean active);

}
