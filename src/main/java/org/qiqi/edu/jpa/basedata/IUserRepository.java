package org.qiqi.edu.jpa.basedata;

import org.qiqi.edu.entity.basedata.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IUserRepository extends JpaRepository<UserEntity, String> {
}
