package org.qiqi.edu.jpa.student;

import org.qiqi.edu.entity.student.SignUpApplyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public interface ISignUpApplyRepository extends JpaRepository<SignUpApplyEntity, String>, JpaSpecificationExecutor<SignUpApplyEntity> {

}
