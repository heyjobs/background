package org.qiqi.edu.jpa.student;

import org.qiqi.edu.entity.student.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public interface IStudentRepository extends JpaRepository<StudentEntity, String>, JpaSpecificationExecutor<StudentEntity> {
    List<StudentEntity> findByClassId(String classId);
}
