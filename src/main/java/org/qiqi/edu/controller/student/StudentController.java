package org.qiqi.edu.controller.student;

import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.student.StudentEntity;
import org.qiqi.edu.service.student.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@RestController
public class StudentController {
    private final IStudentService studentService;

    @Autowired
    public StudentController(IStudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/student")
    public QiQiResponse students(StudentEntity condition) {
        List<StudentEntity> studentList = studentService.match(condition);
        return QiQiResponse.ok("ok", studentList);
    }

    @GetMapping("/student/{id}")
    public QiQiResponse findById(@PathVariable String id) {
        StudentEntity student = studentService.findById(id);
        return QiQiResponse.ok("ok", student);
    }
}
