package org.qiqi.edu.controller.student;

import org.qiqi.edu.common.QiQiException;
import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.student.SignUpApplyEntity;
import org.qiqi.edu.service.student.ISignUpApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@RestController
public class SignUpApplyController {
    private final ISignUpApplyService signUpApplyService;

    @Autowired
    public SignUpApplyController(ISignUpApplyService signUpApplyService) {
        this.signUpApplyService = signUpApplyService;
    }

    @PostMapping("/signUp")
    public QiQiResponse apply(@RequestBody SignUpApplyEntity apply) throws QiQiException {
        signUpApplyService.save(apply);
        return QiQiResponse.ok();
    }
}
