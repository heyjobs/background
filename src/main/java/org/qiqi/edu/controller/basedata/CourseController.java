package org.qiqi.edu.controller.basedata;

import org.apache.commons.lang3.StringUtils;
import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.basedata.CourseEntity;
import org.qiqi.edu.service.basedata.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@RestController
public class CourseController {

    private final ICourseService courseService;

    @Autowired
    public CourseController(ICourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/course")
    public QiQiResponse course(CourseEntity condition) {
        List<CourseEntity> match = courseService.match(condition);
        return QiQiResponse.ok("ok", match);
    }

    @GetMapping("/course/all")
    public QiQiResponse course() {
        List<CourseEntity> all = courseService.findAll();
        return QiQiResponse.ok("ok", all);
    }
}
