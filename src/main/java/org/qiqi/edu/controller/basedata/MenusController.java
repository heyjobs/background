package org.qiqi.edu.controller.basedata;

import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.basedata.MenusEntity;
import org.qiqi.edu.service.basedata.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@RestController
public class MenusController {

    private final IMenuService menuService;

    @Autowired
    public MenusController(IMenuService menuService) {
        this.menuService = menuService;
    }

    @GetMapping("/menus")
    public QiQiResponse menus() {
        List<MenusEntity> menus = menuService.findByActive(true);
        return QiQiResponse.ok("ok", menus);
    }
}
