package org.qiqi.edu.controller.basedata;

import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.basedata.ClassEntity;
import org.qiqi.edu.service.basedata.IClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@RestController
public class ClassController {

    private final IClassService classService;

    @Autowired
    public ClassController(IClassService classService) {
        this.classService = classService;
    }

    @GetMapping("/classes")
    public QiQiResponse classes(ClassEntity condition) {
        List<ClassEntity> match = classService.match(condition);
        return QiQiResponse.ok("ok", match);
    }
}
