package org.qiqi.edu.controller.basedata;

import org.qiqi.edu.common.QiQiResponse;
import org.qiqi.edu.entity.basedata.UserEntity;
import org.qiqi.edu.service.basedata.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@RestController
public class UserController {
    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public QiQiResponse user(@RequestBody UserEntity user) {
        userService.save(user);
        return QiQiResponse.ok();
    }
}
