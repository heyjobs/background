package org.qiqi.edu.utils.verify;

import org.apache.commons.lang3.StringUtils;
import org.qiqi.edu.common.QiQiException;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public class VerifyUtils {
    public static void isBlack(VerifyMsg verifyMsg, String... text) throws QiQiException {
        for (String item : text) {
            if (StringUtils.isBlank(item)) {
                throw new QiQiException(verifyMsg.toString());
            }
        }
    }

    public static void isBlack(String text) throws QiQiException {
        isBlack(VerifyMsg.build("字符串为空或空字符"), text);
    }
}
