package org.qiqi.edu.utils.verify;

import lombok.Setter;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@Setter
public class VerifyMsg {
    private String msg;

    private VerifyMsg(String msg) {
        this.msg = msg;
    }

    public static VerifyMsg build(String msg) {
        return new VerifyMsg(msg);
    }

    @Override
    public String toString() {
        return msg;
    }
}
