package org.qiqi.edu.utils;

import java.util.UUID;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public class IdUtils {
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
