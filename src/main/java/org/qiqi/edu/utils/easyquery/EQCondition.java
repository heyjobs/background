package org.qiqi.edu.utils.easyquery;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
public enum EQCondition {
    /**
     * 模糊查询
     */
    like,
    preLike,
    postLike,
    /**
     * 相等查询
     */
    equal,
    /**
     * 大于等于
     */
    greaterThanOrEqualTo,
    /**
     * 大于
     */
    greaterThan,
    /**
     * 小于等于
     */
    lessThanOrEqualTo,
    /**
     * 小于
     */
    lessThan
}