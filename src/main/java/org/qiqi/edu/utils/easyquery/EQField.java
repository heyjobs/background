package org.qiqi.edu.utils.easyquery;


import java.lang.annotation.*;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface EQField {
    EQCondition[] conditions() default EQCondition.equal;
}
