package org.qiqi.edu.entity.basedata;

import lombok.Data;
import org.qiqi.edu.utils.easyquery.EQField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author qiqiang
 * @date 2019-02-16
 */
@Data
@Entity
@Table(name = "course")
public class CourseEntity {
    /**
     * id
     */
    @Id
    @Column(length = 50)
    private String courseId;
    /**
     * 名称
     */
    @Column(length = 50)
    private String courseName;
    /**
     * 年级
     */
    @Column(length = 50)
    @EQField
    private String grade;
}
