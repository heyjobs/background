package org.qiqi.edu.entity.basedata;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 菜单
 *
 * @author qiqiang
 * @date 2019-02-15
 */
@Data
@Entity
@Table(name = "menus")
public class MenusEntity {
    @Id
    @Column(length = 50)
    private String id;
    @Column(length = 100)
    private String icon;
    @Column(length = 100)
    private String url;
    @Column(length = 100)
    private String name;
    @Column(length = 2)
    private Integer sort;
    /**
     * 是否启用
     */
    @Column(length = 2)
    private Boolean active;
}
