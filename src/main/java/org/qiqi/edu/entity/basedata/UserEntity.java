package org.qiqi.edu.entity.basedata;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Data
@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @Column(length = 40)
    private String openId;
    @Column(length = 50)
    private String nickName;
    @Column(length = 10)
    private String role;
}
