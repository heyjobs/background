package org.qiqi.edu.entity.basedata;

import lombok.Data;
import org.qiqi.edu.entity.student.StudentEntity;
import org.qiqi.edu.utils.easyquery.EQCondition;
import org.qiqi.edu.utils.easyquery.EQField;

import javax.persistence.*;
import java.util.List;

/**
 * 班级
 *
 * @author qiqiang
 * @date 2019-02-15
 */
@Data
@Entity
@Table(name = "class")
public class ClassEntity {

    @Id
    @Column(length = 10)
    private String id;

    /**
     * 年级
     */
    @Column(length = 50)
    @EQField
    private String grade;
    /**
     * 班级名称
     */
    @Column(length = 50)
    @EQField(conditions = EQCondition.like)
    private String name;

    /**
     * 班级编号
     */
    @Column(length = 50)
    @EQField
    private String number;

    /**
     * 班主任
     */
    @Column(length = 50)
    @EQField
    private String masterTeacher;

    @Transient
    private List<StudentEntity> studentList;


}
