package org.qiqi.edu.entity.student;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 报名申请
 *
 * @author qiqiang
 * @date 2019-02-16
 */
@Data
@Entity
@Table(name = "signUpApply")
public class SignUpApplyEntity {
    @Id
    @Column(length = 50)
    private String applyId;

    @Column(length = 50)
    private String openId;

    /**
     * 申请人姓名
     */
    @Column(length = 50)
    private String name;
    /**
     * 手机号
     */
    @Column(length = 50)
    private String phone;

    /**
     * 家庭住址
     */
    @Column(length = 100)
    private String address;
    /**
     * 就读学校
     */
    @Column(length = 50)
    private String school;
    /**
     * 所就读班级
     */
    @Column(length = 50)
    private String schoolClass;

    /**
     * 申请补习年级
     */
    @Column(length = 50)
    private String grade;

    /**
     * 申请补习科目
     */
    @Column(length = 50)
    private String courseId;
    @Column(length = 50)
    private String courseName;
    /**
     * 学费
     */
    @Column(length = 50, columnDefinition = "decimal")
    private BigDecimal schoolFee;
    @Column
    private Date signUpDate;
}
