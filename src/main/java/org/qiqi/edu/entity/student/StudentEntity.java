package org.qiqi.edu.entity.student;

import lombok.Data;
import org.qiqi.edu.utils.easyquery.EQCondition;
import org.qiqi.edu.utils.easyquery.EQField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Data
@Table(name = "student")
@Entity
public class StudentEntity {
    @Id
    @Column(length = 50)
    private String id;
    @Column(length = 50)
    @EQField(conditions = EQCondition.like)
    private String name;
    @Column(length = 50)
    private Boolean sex;
    @Column(length = 50)
    @EQField
    private String classId;
    @Column(length = 11)
    private String phone;
    @Column(length = 40)
    @EQField
    private String openId;
    /**
     * 报名时间
     */
    private Date registerTime;
}
