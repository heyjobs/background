package org.qiqi.edu.common;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
public enum RoleEnums {
    /**
     * 游客
     */
    guest("游客"),
    /**
     * 学生
     */
    student("学生"),
    /**
     * 老师
     */
    teacher("老师"),
    /**
     * 管理员
     */
    admin("管理员");

    String name;

    RoleEnums(String name) {
        this.name = name;
    }
}
