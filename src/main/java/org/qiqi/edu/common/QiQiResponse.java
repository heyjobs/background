package org.qiqi.edu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qiqiang
 * @date 2019-02-15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QiQiResponse<T> {
    private int code;
    private String message;
    private T data;

    public static QiQiResponse ok() {
        return new QiQiResponse<>(0, "ok", null);
    }

    public static QiQiResponse ok(String message) {
        return new QiQiResponse<>(0, message, null);
    }

    public static <T> QiQiResponse ok(String message, T data) {
        return new QiQiResponse<>(0, message, data);
    }

    public static <T> QiQiResponse error(String message) {
        return new QiQiResponse<>(-1, message, null);
    }
}
