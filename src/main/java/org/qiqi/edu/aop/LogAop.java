package org.qiqi.edu.aop;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * web请求日志切面
 *
 * @author 作草分茶
 * @date 2018-08-09
 */
@Aspect
@Component
@Order(1)
@Slf4j
public class LogAop {
    /**
     * 记录开始请求的时间
     */
    private static ThreadLocal<Long> startTime = new ThreadLocal<>();

    @Pointcut("execution(public * org.qiqi..*Controller..*(..))")
    public void webLog() {
    }

    /**
     * 在处理请求前记录
     *
     * @param joinPoint
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        if (sra == null) {
            return;
        }
        startTime.set(System.currentTimeMillis());
        HttpServletRequest request = sra.getRequest();
        log.info("URL :{} ", request.getRequestURL().toString());
        log.info("IP :{}", request.getRemoteAddr());
        log.info("ARGS :{} ", Arrays.toString(joinPoint.getArgs()));
    }

    /**
     * 处理请求结束日志记录
     *
     * @param object
     * @throws Throwable
     */
    @AfterReturning(returning = "object", pointcut = "webLog()")
    public void doAfterReturning(Object object) {
        log.info("RESPONSE : {}", JSONObject.toJSONString(object));
        log.info("HANDLE_TIME : {}", +(System.currentTimeMillis() - startTime.get()));
        startTime.remove();
    }
}